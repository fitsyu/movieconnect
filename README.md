**MovieConnect**
_by Fitsyu2_


## Checklist:

[x] Use Swift 3 or higher

[x] Use VIPER development pattern

[ ] Implement Kingfisher for doanloading and caching image (No, because we use `moa`)

[x] Use Alamofire for networking in Swift

[x] Use Auto Layout in iOS

[x] Use Cocoapods for dependency manager in Swift

## DONE:

- show list of movie from tmdb
- show a movie detail

## TODO:

- sort by popularity
- sort by rating

_Thanks..