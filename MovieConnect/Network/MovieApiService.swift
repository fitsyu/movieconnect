//
//  MovieApiService.swift
//  MovieConnect
//
//  Created by Fitsyu on 23/11/2018.
//  Copyright © 2018 o0o. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import RxSwift

class MoviesApiService {
    
    static func fetchMovies() -> Observable<[Movie]> {
        print("fetching from ",Endpoints.Movies.list.url)
        return Observable<[Movie]>.create { observer -> Disposable in
            let request = Alamofire
                .request(Endpoints.Movies.list.url, method: .get)
                .validate()
                .responseObject(completionHandler: { (response: DataResponse<MovieContainer>) in
                    
                    switch response.result
                    {
                    case .success(let obj):
                        observer.onNext( obj.movies )
                        observer.onCompleted()
                        
                    case .failure(let error):
                        observer.onError(error)
                    }
                    
                    
                })
            
            return Disposables.create(with: {
                request.cancel()
            })
        }
    }
    
    static func fetchMovieDetailsById(id: Int) -> Observable<Movie> {
        
        print("fetching from ",Endpoints.Movies.detail(id).url)
        
        return Observable<Movie>.create { observer -> Disposable in
            let request = Alamofire
                .request(Endpoints.Movies.detail(id).url, method: .get)
                .validate()
                .responseObject(completionHandler: { (response: DataResponse<Movie>) in
                    
                    switch response.result
                    {
                    case .success(let obj):
                        observer.onNext( obj )
                        observer.onCompleted()
                        
                    case .failure(let error):
                        observer.onError(error)
                    }
                    
                    
                })
            
            return Disposables.create(with: {
                request.cancel()
            })
        }
    }
}
