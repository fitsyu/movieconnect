//
//  Endpoints.swift
//  MovieConnect
//
//  Created by Fitsyu on 23/11/2018.
//  Copyright © 2018 o0o. All rights reserved.
//

import Foundation

struct API {
    static let baseUrl = "https://api.themoviedb.org/3"
    static let key     = "dd108336bf3e9ba6c93209b35c0b9214"
}

protocol Endpoint {
    var path: String { get }
    var url: String { get }
}

enum Endpoints {
    
    enum Movies: Endpoint
    {
        case list
        case detail(Int)
        case image(String)
        
        public var path: String {
            switch self
            {
            case .list:
                return "/discover/movie"
                
            case .detail:
                return "/movie"
                
            case .image:
                return "https://image.tmdb.org/t/p/w185"
            }
        }
        
        public var url: String
        {
            switch self {
            case .list:
                return "\(API.baseUrl)\(path)?api_key=\(API.key)"
                
            case .detail(let id):
                return "\(API.baseUrl)\(path)/\(id)?api_key=\(API.key)"
                
            case .image(let imgPath):
                return "\(path)\(imgPath)"
            }
        }
    }
}
