//
//  Router.swift
//  MovieConnect
//
//  Created by Fitsyu on 23/11/2018.
//  Copyright © 2018 o0o. All rights reserved.
//

import UIKit
import PKHUD

class MoviesRouter: MoviesWireframe {
    
    weak var viewController: UIViewController?
    
    static func assembleModule() -> UIViewController {
        
        let nvc = R.storyboard.movie.instantiateInitialViewController()
        
        let view =  nvc!.viewControllers.first as! MovieListViewController
        let presenter = MoviesPresenter()
        let interactor = MoviesInteractor()
        let router = MoviesRouter()
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        
        router.viewController = view
        
        return nvc!
    }

    
    func presentDetails(forMovie movie: Movie) {

        // show movie in next module
        
        let vc = MovieDetailsRouter.assembleModule(movie)
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
