//
//  View.swift
//  MovieConnect
//
//  Created by Fitsyu on 23/11/2018.
//  Copyright © 2018 o0o. All rights reserved.
//

import UIKit
import PKHUD

class MovieListViewController: UIViewController {
    
    
    @IBOutlet weak var moviesCollectionView: UICollectionView!
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    
    var presenter: MoviesPresentation!
    var movies: [Movie] = [] {
        didSet {
            moviesCollectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionViewLayout()
        
        presenter.viewDidLoad()
        
        HUD.show(.labeledProgress(title: "", subtitle: "Please wait"))
    }
    
    fileprivate func setupCollectionViewLayout()
    {
        let screenWidth = UIScreen.main.bounds.width - 2
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        
        let itemWidth = screenWidth / 2
        let itemHeight = itemWidth * 1.25
        
        layout.itemSize =     CGSize(width: itemWidth, height: itemHeight)
        layout.minimumInteritemSpacing  = 1
        layout.minimumLineSpacing       = 1
    }
    
    
    @objc fileprivate func didClickSortButton(_ sender: Any?) {
        presenter.didClickSortButton()
    }
}

extension MovieListViewController: MoviesView {
    
    func showNoContentScreen() {
        HUD.hide(afterDelay: 1, completion: { _ in })
    }
    
    func showMoviesData(_ movies: [Movie]) {
        HUD.hide(afterDelay: 1, completion: { _ in
            self.movies = movies
        })
    }
}

extension MovieListViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
//        R.reuseIdentifier == 0 ???
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell" , for: indexPath) as! MovieCell
        
        cell.data = movies[indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selected = movies[indexPath.item]
        presenter.didSelectMovie(selected)
    }
}

import moa
class MovieCell: UICollectionViewCell
{
    @IBOutlet weak var poster: UIImageView!
    @IBOutlet weak var title: UILabel!
    
    var data: Movie? {
        didSet {
            guard let data = data else { return }
            
            poster.moa.url = data.posterPath
            title.text = data.title
        }
    }
}




