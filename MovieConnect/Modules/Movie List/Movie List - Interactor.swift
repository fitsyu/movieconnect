//
//  Interactor.swift
//  MovieConnect
//
//  Created by Fitsyu on 23/11/2018.
//  Copyright © 2018 o0o. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import RxSwift

class MoviesInteractor: MoviesUseCase {
    
    weak var output: MoviesInteractorOutput!
    private var disposeBag = DisposeBag()
    
    func fetchMovies() {
        MoviesApiService
            .fetchMovies()
            .subscribe(
                onNext: { movies in
                    self.output.moviesFetched(movies)
            },
                onError: { error in
                    self.output.moviesFetchFailed()
            }
            )
            .addDisposableTo(disposeBag)
    }
}
