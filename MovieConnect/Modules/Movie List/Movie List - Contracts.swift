//
//  Contracts.swift
//  MovieConnect
//
//  Created by Fitsyu on 23/11/2018.
//  Copyright © 2018 o0o. All rights reserved.
//

import UIKit

protocol MoviesView: class {
    var presenter: MoviesPresentation! { get set }
    
    func showNoContentScreen()
    func showMoviesData(_ movies: [Movie])
}

protocol MoviesPresentation: class {
    var view: MoviesView? { get set }
    var interactor: MoviesUseCase! { get set }
    var router: MoviesWireframe! { get set }
    
    func viewDidLoad()
    func didClickSortButton()
    func didSelectMovie(_ movie: Movie)
}

protocol MoviesUseCase: class {
    var output: MoviesInteractorOutput! { get set }
    
    func fetchMovies()
}

protocol MoviesInteractorOutput: class {
    func moviesFetched(_ movies: [Movie])
    func moviesFetchFailed()
}

protocol MoviesWireframe: class {
    var viewController: UIViewController? { get set }
    
    func presentDetails(forMovie movie: Movie)
    
    static func assembleModule() -> UIViewController
}
