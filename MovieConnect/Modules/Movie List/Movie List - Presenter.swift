//
//  Presenter.swift
//  MovieConnect
//
//  Created by Fitsyu on 23/11/2018.
//  Copyright © 2018 o0o. All rights reserved.
//

import Foundation

class MoviesPresenter: MoviesPresentation {
    
    weak var view: MoviesView?
    var interactor: MoviesUseCase!
    var router: MoviesWireframe!
    
    var movies: [Movie] = [] {
        didSet {
            if movies.count > 0 {
                view?.showMoviesData(movies)
            } else {
                view?.showNoContentScreen()
            }
        }
    }
    
    func viewDidLoad() {
        interactor.fetchMovies()
    }
    
    func didClickSortButton() {
        
    }
    
    func didSelectMovie(_ movie: Movie) {
        router.presentDetails(forMovie: movie)
    }
    
}

extension MoviesPresenter: MoviesInteractorOutput {
    
    func moviesFetched(_ movies: [Movie]) {
        self.movies = movies
    }
    
    internal func moviesFetchFailed() {
        view?.showNoContentScreen()
    }
}
