//
//  Module Launcher.swift
//  MovieConnect
//
//  Created by Fitsyu on 23/11/2018.
//  Copyright © 2018 o0o. All rights reserved.
//

import UIKit

class ModuleLauncher {
    
    
    
    class func launch(router: MoviesWireframe.Type,  in window: UIWindow) {
        window.makeKeyAndVisible()
        window.rootViewController = router.assembleModule()
    }
}
