//
//  Interactor.swift
//  MovieConnect
//
//  Created by Fitsyu on 23/11/2018.
//  Copyright © 2018 o0o. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import RxSwift

class MovieDetailsInteractor: MovieDetailsUseCase {
    var output: MovieDetailsInteractorOutput?
    
    private var disposeBag = DisposeBag()
    
    
    func fetchMovieDetailsById(id: Int)  {
        MoviesApiService
            .fetchMovieDetailsById(id: id)
            .subscribe(
                onNext: { movie in
                    self.output?.movieDetailsFetched(movie)
            },
                onError: { error in
                    self.output?.movieDetailsFetchFailed()
            }
            )
            .addDisposableTo(disposeBag)
    }
}

