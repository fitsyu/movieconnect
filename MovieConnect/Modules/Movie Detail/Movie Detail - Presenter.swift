//
//  Presenter.swift
//  MovieConnect
//
//  Created by Fitsyu on 23/11/2018.
//  Copyright © 2018 o0o. All rights reserved.
//

import Foundation

class MovieDetailsPresenter : MovieDetailsPresentation {
    var interactor: MovieDetailsUseCase?
    var router: MovieDetailsWireframe?
    
    weak var view: MovieDetailsView?
    var wireframe: MovieDetailsWireframe!
    var movie: Movie!
    
    func viewDidLoad() {
        guard let id = movie.id else {
            view?.showError(message: "A movie must has id")
            return
        }
        
        interactor?.fetchMovieDetailsById(id: id)
    }
}

extension MovieDetailsPresenter: MovieDetailsInteractorOutput {
    func movieDetailsFetched(_ movie: Movie) {
        view?.showMovieDetails(forMovie: movie)
    }
    
    func movieDetailsFetchFailed() {
        view?.showError(message: "Failed to fetch movie details")
    }
    
    
}

