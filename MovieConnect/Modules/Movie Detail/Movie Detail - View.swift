//
//  View.swift
//  MovieConnect
//
//  Created by Fitsyu on 23/11/2018.
//  Copyright © 2018 o0o. All rights reserved.
//

import UIKit
import moa
import PKHUD

class MovieDetailsViewController: UIViewController, MovieDetailsView
{
    var presenter: MovieDetailsPresentation!
    
    @IBOutlet weak var poster: UIImageView!
    @IBOutlet weak var txRate: UILabel!
    @IBOutlet weak var txTitle: UILabel!
    @IBOutlet weak var txOverview: UITextView!
    
    var movieHomePage: URL?
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        presenter.viewDidLoad()
        HUD.show(.labeledProgress(title: "", subtitle: "Please wait"))
    }
    
    func showMovieDetails(forMovie movie: Movie) {
        
        HUD.hide({ _ in
            if let path = movie.posterPath {
                self.poster.moa.url = path
            }
            
            self.txRate.text =  "\(movie.voteAverage ?? 0)/10"
            self.txTitle.text = movie.title
            self.txOverview.text = movie.overview
            
            if let hompage = movie.homePage {
                self.movieHomePage = URL(string: hompage)
            }
            
        })

    }
    
    func showError(message: String) {
        HUD.show(.labeledError(title: "Error", subtitle: message))
        HUD.hide(afterDelay: 0.25)
    }
    
    @IBAction func btBuy_press(_ sender: UIButton)
    {
        if let homepage = movieHomePage {
            UIApplication.shared.open(homepage)
        } else {
            HUD.show(.labeledError(title: "", subtitle: "No homepage for this movie"))
            HUD.hide(afterDelay: 1)
        }
    }
}
