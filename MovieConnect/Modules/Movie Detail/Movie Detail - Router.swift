//
//  Router.swift
//  MovieConnect
//
//  Created by Fitsyu on 23/11/2018.
//  Copyright © 2018 o0o. All rights reserved.
//

import UIKit

class MovieDetailsRouter: MovieDetailsWireframe {
    
    var viewController: UIViewController?
    
    static func assembleModule(_ movie: Movie) -> UIViewController {
        let view = R.storyboard.movie().instantiateViewController(withIdentifier: "MovieDetailsViewController") as! MovieDetailsViewController
        
        let presenter = MovieDetailsPresenter()
        let interactor = MovieDetailsInteractor()
        let router = MovieDetailsRouter()
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.movie = movie
        
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        
        router.viewController = view
        
        return view
    }
}
