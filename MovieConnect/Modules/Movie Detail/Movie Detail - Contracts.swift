//
//  Contracts.swift
//  MovieConnect
//
//  Created by Fitsyu on 23/11/2018.
//  Copyright © 2018 o0o. All rights reserved.
//

import UIKit

protocol MovieDetailsView: class {
    var presenter: MovieDetailsPresentation! { get set }
    
    func showMovieDetails(forMovie movie: Movie)
    func showError(message: String)
}

protocol MovieDetailsPresentation: class {
    var view: MovieDetailsView? { get set }
    var interactor: MovieDetailsUseCase? { get set }
    var router: MovieDetailsWireframe? { get set }
    
    func viewDidLoad()
}

protocol MovieDetailsUseCase: class {
    var output: MovieDetailsInteractorOutput? { get set }
    
    func fetchMovieDetailsById(id: Int)
}

protocol MovieDetailsInteractorOutput: class {
    func movieDetailsFetched(_ movie: Movie)
    func movieDetailsFetchFailed()
}

protocol MovieDetailsWireframe: class {
    var viewController: UIViewController? { get set }
    
    static func assembleModule(_ movie: Movie) -> UIViewController
}

