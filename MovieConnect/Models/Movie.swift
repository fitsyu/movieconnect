//
//  Movie.swift
//  MovieConnect
//
//  Created by Fitsyu on 23/11/2018.
//  Copyright © 2018 o0o. All rights reserved.
//

import Foundation
import ObjectMapper

struct Movie
{
    var id: Int?
    var title: String?
    var overview: String?
    var popularity: Double?
    var voteAverage: Double?
    var homePage: String?
    
    var posterPath: String?
    {
        didSet
        {
            posterPath = Endpoints.Movies.image(posterPath!).url
        }
    }
}

extension Movie: Mappable
{
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        id              <- map["id"]
        title           <- map["title"]
        overview        <- map["overview"]
        popularity      <- map["popularity"]
        voteAverage     <- map["vote_average"]
        posterPath      <- map["poster_path"]
        homePage        <- map["homepage"]
    }
}


class MovieContainer: Mappable
{
    var movies: [Movie] = []
    
    required init?(map: Map) {}
    func mapping(map: Map) {
        movies <- map["results"]
    }
}
